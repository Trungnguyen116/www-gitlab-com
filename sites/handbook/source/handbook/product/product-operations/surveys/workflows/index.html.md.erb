---
layout: handbook-page-toc
title: Product Operations Survey Workflows
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Post purchase survey (PPS)

### How PPS works

The post purchase survey (PPS) is launched in [Qualtrics](https://www.qualtrics.com/) via Salesforce. Currently, any time a sales assisted contract is confirmed in Salesforce, Qualtrics is triggered to send out a survey to the customer contact on that subscription. The feature list in Qualtrics is maintained via [monthly issues](https://gitlab.com/gitlab-com/Product/-/issues/2561) which leverage the [feature by tier page](https://about.gitlab.com/features/by-paid-tier/). The feature by tier page is auto generated from [features.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/features.yml). The quarterly scores from the PPS read-outs below flow into features.yml via the process, which is owned by Product Operations. We are working on flowing web direct and renewal subscriptions into this survey as well in FY22.

### Current workflow 

#### Automated tasks

1. SFDC triggers a Qualtrics workflow:
  - Sales-assisted new subscription purchases
  - Qualtrics sends an email with one survey question
  - Survey question is personalized by plan type
  - "What <plan> features contributed to your decision to purchase a subscription? You can select up to 5."
1. On the first day of a new quarter, the bot generates an "[FYXX QY Post Purchase Survey Analysis](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/ProdOps-PPS-Analysis.md)" to drive the manual tasks.
1. One month prior to the end of the quarter, the bot generates an "[FYXX QY PPS Feature List Update](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/ProdOps-PPS-Feature-List-Update.md)" issue to drive the manual tasks to update the feature list that is shown after the survey question mentioned above.

#### Manual tasks

1. The manual tasks are described in the Issue templates and are the SSOT for carrying out the PPS
  - [PPS Analysis](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/ProdOps-PPS-Analysis.md)
  - [PPS Feature List Update](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/ProdOps-PPS-Feature-List-Update.md)

#### Deployment

#### Updating `PPS` data

It is important we keep pricing related data in `features.yml` up to date because it affects pricing decisions and auto-generated pages that are referenced across various teams at GitLab, including Product Pricing, Marketing and Sales. [Post-purchase survey](https://about.gitlab.com/direction/product-operations/#post-purchase) (PPS) data is taken monthly via Qualtrics. The output should come in the form of a spreadsheet which should look something like [this (internal only)](https://docs.google.com/spreadsheets/d/1Z_oyudSJeZR68VgrM87ePfZmVfUnOIgQrr1iWzmImH0/edit#gid=1021405695) and is shared out via the [quarterly analysis issues](https://gitlab.com/gitlab-com/Product/-/issues/2272).

In that spreadsheet, the `feature` column matches to a feature in `data/features.yml`. That's what needs to get updated. There are two fields which we are updating here:

1. `pps_aggregate` represents the total of all PPS scores from all time.
1. `pps_recent` represents the _most recent_ PPS result. If a feature is not listed in the results, the "recent" value should be marked as `0`.

Every feature in `features.yml` _should have both entries_. If it's a brand-new feature, it hasn't been surveyed in a PPS yet, the values are still zero.

#### *How to update:*

<iframe width="560" height="315" src="https://www.youtube.com/embed/XG917ZHKnQE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

1. Open [`data/features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/features.yml)
1. For each row on the "SUMMARY_alltime" sheet, you'll update the `pps_aggregate` value to match that new value.
1. In `features.yml`, find-and-replace all the values to zero. In an IDE, you can use a regular expression like `pps_recent: \d*` and replace it with `pps_recent: 0`.
1. For each row on the "SUMMARY_Qx" sheet (the one from the current quarter), you'll update the `pps_recent` value to match that new value.
1. Submit your MR, and merge! 
     To see a sample, check out the diff in the FY22 Q1 update MR [here](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/85089/).

## Paid NPS survey (PNPS)

### How PNPS works
Product Operations and UX Research partner on running PNPS surveys. We determine our Net Promoter Score for paid users of GitLab.com (SaaS) on a quarterly basis through a survey launched via Qualtrics. A [data collection and analysis issue](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/ProdOps-PNPS-Collection-Analysis.md) ([example](https://gitlab.com/gitlab-com/Product/-/issues/5025)) is automatically created at the beginning of each quarter which is the SSOT and outlines all tasks, DRI's and due dates. Data is collected over a period of six weeks starting in the middle of a quarter and the survey stays open until the last day of the quarter. The first two weeks of the new quarter are used to analyze and report data on the previous quarter. All documents created are stored in the PNPS drive.

### Current workflow 

#### Sample goals
We aim for a sample of over 500 with plan type proportions for our sample to be +/- 3% compared to the population proportion. 

#### Data Analysis
Data analysis consists of two components: statistical analysis and open response coding.

1. Statistical analysis: The majority of the statistical analysis is accomplished by the [analysis template](https://docs.google.com/spreadsheets/d/1Y7iOAqQBidrx1b4XrU-VpZuCYWuEUIVql6eAf1V5xuM/edit#gid=1906724493). Drop the results CSV into it to generate the data provided in the results deck. Follow the instructions in the template.

2. Open response coding: There are instructions on how to code the responses in the [analysis template](https://docs.google.com/spreadsheets/d/1Y7iOAqQBidrx1b4XrU-VpZuCYWuEUIVql6eAf1V5xuM/edit#gid=1906724493).

Review [this video](https://www.youtube.com/watch?v=Exz74ynftMo) for a detailed walkthrough of running the analysis.

3. Responder Outreach: We give survey respondents the option to indicate whether they'd be open to a follow-up contact when filling in the survey. As part of the analysis, a list of users that agreed to the follow-up is generated. Copy this list to a new spreadsheet and share it with Product Operations who leads and kicks off the responder outreach.

#### Deployment 

##### Generating a list of eligible users
At the beginning of each quarter a list of eligible users to send the survey to is generated, using the following steps:
1. Remove users that were contacted more than 12 months ago from [the list of previously contacted users](https://docs.google.com/spreadsheets/d/1Q9xOh5L9QJGdW9tq89rcADvo5jozRxdo7KiO2z0HLAU/edit#gid=0).
2. Generate a list of new eligible users using the [standard NPS user query](https://app.periscopedata.com/app/gitlab:safe-dashboard/919244/Growth-UXR-Scratch?widget=12667049&udv=0). Query for approximately 22k user IDs. Create a new [spreadsheet for this list of users](https://docs.google.com/spreadsheets/d/1sNIBrnH6YoYPCC9UIBZKMnbzAskXbfH0imLNVdu6iLI/edit#gid=0) to track contacts for the quarter.
3. Calculate the [proportions of SaaS users by plan type](https://app.periscopedata.com/app/gitlab:safe-dashboard/919360/TD:-Licensed-Users-by-Product-&-Rate-Plan-Name) for the current quarter. Add these percentages to the user list to help calculate how many users for each plan type you need to invite. The end goal is to achieve a sample breakdown that roughly matches the population's breakdown (+/- 3%).

##### Sending an email wave
1. Using the percentages you calculated, determine how many users for each plan type you need to contact for the wave. If it’s the first wave, use the population proportions. If it’s a subsequent wave, use the proportions you calculate based on the responses so far (see next point).
2. To calculate your current sample plan proportions, download your survey results from Qualtrics. Delete everything but the current quarter. Calculate the percentage breakdown of plans so far. Then subtract that number from the population percentage, and then add the result to the population percentage. 

An example with fake numbers: 
- Population percentage for Ultimate = 73%
- Percentage of Ultimate plan types after sending wave 1 = 65%
- wave 2 percentage for Ultimate: (73% - 65%) + 73% = 81%

In this example the sample is under the population, hence the next wave percentage is higher than the population to try and make that up. 
 
3. Waves should be ~6000 users. Mark the desired number of users out of that 6000 that fit your percentages for each plan type with the name of the wave you are sending.
4. Paste all the user IDs for that wave into a new sheet, following the [process for uploading user IDs into Qualtrics](https://about.gitlab.com/handbook/engineering/ux/qualtrics/#distributing-your-survey-to-gitlabcom-users).
5. Once the mailing list has been created in Qualtrics double-check that no @gitlab.com email addresses were included, and create a new email distribution for the SaaS PNPS survey in Qualtrics using the NPS_v5 template. **Make sure to edit the advanced survey options to set the link expiration to the last day of the quarter**. 
6. Typically emails are scheduled to go out Tuesday - Thursday early in the morning US time (6 - 7am Mountain Time) with the goal of maximizing visibility and responses, but this is far from scientific and open to change. Qualtrics supports scheduling email ahead of time.

Review [this video](https://www.youtube.com/watch?v=nycuSnJ7C7Y&amp;index=61) for a detailed walkthrough of how to send out email waves.


Once all email waves have been sent, add the user IDs that were used this quarter to the previously contacted sheet, noting the quarter. This allows us to avoid contacting the same users too frequently. 

### Outreach to `PNPS` users

After the quarterly analysis is shared out, Product Operation initiates outreach to PNPS responders who opted into contact via this [automated issue](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/ProdOps-PNPS-Responder-Outreach.md).  The full PNPS responder outreach workflow can be seen [here](https://about.gitlab.com/handbook/product/product-processes/#pnps-responder-outreach).

### Displaying the data

Product Operations partners with UX Research to produce and share out [quarterly analysis slides](https://about.gitlab.com/handbook/product/product-operations/surveys/#paid-nps-pnps). Product Operations partners with Product Analytics to maintain the [PNPS dashboards](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/#paid-net-promoter-score-pnps). 

#### PNPS charts and maintenance

There are two charts in Sisense that we reference when analyzing the PNPS results:

- [Score by quarter](https://app.periscopedata.com/app/gitlab/1050043/PNPS-Dashboard?widget=14974938)
- [Current quarter detractor, passive, promoter](https://app.periscopedata.com/app/gitlab/1050043/PNPS-Dashboard?widget=14974956)

They can also [be found in the Product Performance Indicators section of the internal handbook](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/#paid-net-promoter-score-pnps).

The survey responses in Qualtrics are synced to the `qualtrics_nps_scores` table which populates these charts.

#### Excluding Responses

There may be an occasional need to remove certain records (such as internal test records) from the data. The `pnps_excluded_response_ids` Sisense snippet is used to manage these excluded `response_id`s. When a response has to be excluded, go to [edit the PNPS Excluded Response IDs snippet](https://app.periscopedata.com/app/gitlab/snippet/pnps_excluded_response_ids/558761ad2812415aab07b2efafe46ff5/edit) and add the `response_id` to the list. Make sure to match the formatting of the other response IDs and eliminate any spaces. If you are working on a new chart and would like to incorporate this snippet, you just have to add `WHERE response_id NOT IN ([pnps_excluded_response_ids])` to the WHERE clause.

#### Backtracking Responses

For past reports, there has been a request to see previous plans that detractors have had. For example, of Premium detractors, how many were previously Bronze users. If the need arises to do so, follow the guidance below:

- Make sure you have access to Analysis spreadsheet (Google Sheets) and have Edit access to the [PNPS Dashboard](https://app.periscopedata.com/app/gitlab/1050043/PNPS-Dashboard) .
- Go to the [PNPS Dashboard](https://app.periscopedata.com/app/gitlab/1050043/PNPS-Dashboard) and scroll down to the section titled "User Roles, & Detractor Information." 
- To update the [Template Premium Detractors](https://app.periscopedata.com/app/gitlab/1050043/PNPS-Dashboard) chart:
  - In the PNPS Analysis spreadsheet, in the "Completed" tab, filter the `Q3_NPS_GROUP = 'Detractor'` and `plan = 'Premium'`. 
  - Copy this list of `namespace_id`s and add convert them to a string list (i.e. convert a namespace_id from `3777494` to `'3777494',`) for querying purposes.
  - Back in the PNPS Dashboard, click "Edit Chart" for the [Template Premium Detractors](https://app.periscopedata.com/app/gitlab/1050043/PNPS-Dashboard) chart and replace the list of `namespace_id`s with this new list and save the query. The report will update upon saving.
  - Copy these steps for the [Template Ultimate Detractors chart](https://app.periscopedata.com/app/gitlab/1050043/PNPS-Dashboard?widget=15079183&udv=0), with the only change being to filter the spreadsheet for `plan = 'Ultimate'`.


