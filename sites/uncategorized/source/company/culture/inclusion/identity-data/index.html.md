---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2022-11-30

##### Country Specific Data

| **Country Information** | **Percentages** |
|-------------------------|-----------------|
| Based in JAPAC          |          12.72% |
| Based in EMEA           |          29.77% |
| Based in LATAM          |           1.36% |
| Based in USA & Canada   |          56.15% |
| **Total Team Members**  |     **100.00%** |


##### Gender Data

| **Gender (Global)**     | **Percentages** |
|-------------------------|-----------------|
| Men                     |          64.37% |
| Women                   |          32.25% |
| Other Gender Identities |           3.38% |
| **Total Team Members**  |     **100.00%** |

| **Gender in Management (Global)** | **Percentages** |
|-----------------------------------|-----------------|
| Men in Management                 |          59.87% |
| Women in Management               |          35.53% |
| Other Gender Identities           |           4.61% |
| **Total Team Members**            |     **100.00%** |

| **Gender in Leadership (Global)** | **Percentages** |
|-----------------------------------|-----------------|
| Men in Leadership                 |          60.98% |
| Women in Leadership               |          38.41% |
| Other Gender Identities           |           0.61% |
| **Total Team Members**            |     **100.00%** |

| **Gender in Tech (Global)** | **Percentages** |
|-----------------------------|-----------------|
| Men in Tech                 |          76.40% |
| Women in Tech               |          20.35% |
| Other Gender Identities     |           3.26% |
| **Total Team Members**      |     **100.00%** |

| **Gender in Non-Tech (Global)** | **Percentages** |
|---------------------------------|-----------------|
| Men in Non-Tech                 |          43.71% |
| Women in Non-Tech               |          52.62% |
| Other Gender Identities         |           3.67% |
| **Total Team Members**          |     **100.00%** |

| **Gender in Sales (Global)** | **Percentages** |
|------------------------------|-----------------|
| Men in Sales                 |          66.48% |
| Women in Sales               |          30.23% |
| Other Gender Identities      |           3.30% |
| **Total Team Members**       |     **100.00%** |

##### Underrepresented Groups (URG) Data**

| **URG (US Only)**      | **Percentages** |
|------------------------|-----------------|
| Non-URG                |          80.07% |
| URG                    |          17.26% |
| Did Not Identify       |           2.66% |
| **Total Team Members** |     **100.00%** |

| **URG in Tech (US Only)** | **Percentages** |
|---------------------------|-----------------|
| Non-URG                   |          85.10% |
| URG                       |          13.58% |
| Did Not Identify          |           1.32% |
| **Total Team Members**    |     **100.00%** |

| **URG in Non-Tech (US Only)** | **Percentages** |
|-------------------------------|-----------------|
| Non-URG                       |          76.90% |
| URG                           |          20.56% |
| Did Not Identify              |           2.54% |
| **Total Team Members**        |     **100.00%** |

| **URG in Sales (US Only)** | **Percentages** |
|----------------------------|-----------------|
| Non-URG                    |          79.17% |
| URG                        |          17.13% |
| Did Not Identify           |           3.70% |
| **Total Team Members**     |     **100.00%** |

| **URG in Management (US Only)** | **Percentages** |
|---------------------------------|-----------------|
| Non-URG                         |          84.66% |
| URG                             |          13.07% |
| Did Not Identify                |           2.27% |
| **Total Team Members**          |     **100.00%** |

| **URG in Leadership (US Only)** | **Percentages** |
|---------------------------------|-----------------|
| Non-URG                         |          85.40% |
| URG                             |          13.14% |
| Did Not Identify                |           1.46% |
| **Total Team Members**          |     **100.00%** |


**Due to data and or legal limitations, this is not an exhaustive list of all of our underrepresented groups.  Those with disabilities, those that identify as LGBTQIA+, etc. who choose not to disclose or underrepresented ethnicities outside of the US. 

The DIB Team is actively working on finding data sets outside the US and inclusion metrics for underrepresented groups we cannot report on as team member representation. 

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's People Analytics Team, WorkDay
